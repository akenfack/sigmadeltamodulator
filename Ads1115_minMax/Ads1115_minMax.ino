


#include <Wire.h>
#include <Adafruit_ADS1015.h>
#include <Math.h>

Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */

void setup(void) 
{
  Serial.begin(9600);       // Sets the data rate in bits per second for serial data transmission. 
                            // It is highly recommended to set the same frequency in the waveform generator otherwise the signal will be displayed incorrectly
  Serial.println("Starting the sofware!");
  
  Serial.println("Getting single-ended readings from AIN0..3");
  //Serial.println("ADC Range: +/- 6.144V (1 bit = 3mV/ADS1015, 0.1875mV/ADS1115)");
  
  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
   ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
  
  ads.begin();
}

void loop(void) 
{
  float Amplitude = 5; // We have to assume the aplitude, the highest value. eg from 0 to 5 or from 0 to 6. 
  float allowedExceededError = 0.5; // We are allowing the signal to exceed the given value. Eg 0 to 5 but 5.5 is also allowed and will not scale graph.
  // The results will be adjusted to this setting
  
  int16_t data0;
  float Voltage;
  data0 = ads.readADC_SingleEnded(0); // Read from the Adafruit

  // variables used to find the lowest and the highest value from the ADC
  static float min = 1000000;
  static float max = 0;
  static float i = 0;
  i++;
  static float exceededRange = 0; // If the signal rises and it exceeds our highes value the graph will be scaled

  if(min > data0)                 // Finding the min value
    min = data0;

  if(max < data0)                 // Finding the max value
    max = data0;
 
  Serial.print("AIN0: ");         //Serial.print(data0);
  //Voltage=(0.125/1000)*data0;
  
  static float ratio;
  if(i < 15)                      // Print clear "zero" results to the output while collecting data
  {
    for(int i = 0; i < 10; i++)
    {
      Serial.print("\tVoltage: ");
      Serial.println(0, 2);
    }
  }
  else if(i == 15)
    ratio = Amplitude / (max - min);  // Calculate ratio in order to convert Adafruit data to Voltage
  else if(i > 15)                 // Start plotting results
  {
    Voltage = (data0-min)*ratio;  // Converts ADAFruit data to Voltage from 0 to given value (Amplitude)
    Serial.print("\tVoltage: ");
    Serial.println(Voltage, 2);   // Print voltage with double precision eg. 2,34
  }
  
  if(Voltage > Amplitude && i > 15) // If voltage exceeds set value and is not collecting data (i>15)
  {
    if(Voltage - Amplitude <= allowedExceededError)
    {
      // Do nothing, eg. +-0.5 is allowed to exceed.
    }
    else
    {
      exceededRange++; // Increment counter of times tha range have exceeded
    }
  }
  if(exceededRange > 15)  // If the range was exceeded 15 times, reset and rescale graph
  {
    i = 0;
    min = 1000000;
    max = 0;
    exceededRange = 0;
  }
  
  delay(100); // Delay before repeating the loop (0.1s)
}
